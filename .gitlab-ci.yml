stages:
  - version
  - test
  - deploy
  - release

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For default branch, create a pipeline.
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, never create pipeline.
    - if: '$CI_COMMIT_TAG'
      when: never

Update version:
  stage: version
  image: ruby:2.7.5
  interruptible: true
  before_script:
    - gem install gfsm
  script:
    - gfsm version bump --force > .version
    - gfsm changelog --force --output-file CHANGELOG.md
    - echo "RELEASE_VERSION=$(<.version)" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env
    paths:
      - .version
      - CHANGELOG.md

RSpec:
  stage: test
  image: ruby:2.7.5
  interruptible: true
  before_script:
    - bundle install
  script:
    - bundle exec rspec
  needs:
    - job: "Update version"
      artifacts: true
    
rubocop:
  stage: test
  image: ruby:2.7.5
  interruptible: true
  before_script:
    - bundle install
  script:
    - bundle exec rubocop
  needs:
    - job: "Update version"
      artifacts: true

danger-review:
  stage: test
  image: ruby:2.7.5
  interruptible: true
  needs: []
  before_script:
    - gem install danger-gitlab
    - gem install danger-gfsm_commit_trailer
    - gem install danger-commit_lint
  script:
    - export CI_PROJECT_PATH=$CI_PROJECT_ID
    - danger --fail-on-errors=true
    
# Publish gem:
# - GEM_HOST_API_KEY: A valid RubyGems API key.
Publish gem:
  stage: deploy
  image: ruby:2.7.5
  needs:
    - job: "Update version"
      artifacts: true
    - job: "RSpec"
      artifacts: false
    - job: "rubocop"
      artifacts: false
  variables:
    GEMSPEC_FILE: "danger-gfsm_commit_trailer.gemspec"
    GEM_FILE: "danger-gfsm_commit_trailer-${RELEASE_VERSION}.gem"
  before_script:
    - |
      rm -f ./*.gem
      [ -f "${GEMSPEC_FILE}" ] || (echo "No ${GEMSPEC_FILE} file found!" && exit 1)
  script:
    - |
      printf "# frozen_string_literal: true\n\nmodule GfsmCommitTrailer\n  VERSION = \"$RELEASE_VERSION\"\nend\n" > lib/gfsm_commit_trailer/gem_version.rb
      gem build "${GEMSPEC_FILE}"
      [ -f "${GEM_FILE}" ] || (echo "No ${GEM_FILE} file found!" && exit 1)
      gem push "${GEM_FILE}"
  artifacts:
    paths:
      - "*.gem"
  rules:
    - if: '$GEM_HOST_API_KEY == null'
      when: never
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

Create release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - |
      release-cli create --name "Release v$RELEASE_VERSION" --description CHANGELOG.md --tag-name "v$RELEASE_VERSION"
  needs:
    - job: "Update version"
      artifacts: true
    - job: "Publish gem"
      artifacts: false
  rules:
    - if: '$GEM_HOST_API_KEY == null'
      when: never
    - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'