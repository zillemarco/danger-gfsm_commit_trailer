# danger-gfsm_commit_trailer

Danger plugin to add on projects using GFSM in order to make sure each MR creates a CHANGELOG entry.

## Installation

    $ gem install danger-gfsm_commit_trailer

## Usage

    Methods and attributes from this plugin are available in
    your `Dangerfile` under the `gfsm_commit_trailer` namespace.

## Development

1. Clone this repo
2. Run `bundle install` to setup dependencies.
3. Run `bundle exec rake spec` to run the tests.
4. Use `bundle exec guard` to automatically have tests run as you make changes.
5. Make your changes.
