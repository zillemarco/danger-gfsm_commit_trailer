# frozen_string_literal: true

require File.expand_path("spec_helper", __dir__)

TEST_MESSAGES = {
  no_trailers: [
    "First commit without trailer",
    "Second commit without trailer"
  ],
  single_trailer: [
    "Commit with a trailer\n\nCHANGELOG: fixed",
    "Second commit without trailer"
  ],
  multiple_trailers: [
    "First commit with a trailer\n\nCHANGELOG: fixed",
    "Second commit with a trailer\n\nCHANGELOG: added"
  ],
  trailer_in_subject: [
    "CHANGELOG: added"
  ]
}.freeze

def report_counts(status_report)
  status_report.values.flatten.count
end

def random_sha
  (0...7).map { rand(65...91).chr }.join
end

module Danger
  describe Danger::DangerGfsmCommitTrailer do
    it "should be a plugin" do
      expect(Danger::DangerGfsmCommitTrailer.new(nil)).to be_a Danger::Plugin
    end

    describe "when errors should be trated as errors" do
      describe "where there are no trailers" do
        it "reports errors" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:no_trailers].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(1)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([Danger::DangerGfsmCommitTrailer::ERROR_MESSAGE])
        end
      end

      describe "where there is a single trailer" do
        it "does not report any error" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:single_trailer].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(0)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([])
        end
      end

      describe "where there are multiple trailers" do
        it "does not report any error" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:multiple_trailers].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(0)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([])
        end
      end

      describe "when the trailer is on the subject" do
        it "reports errors" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:trailer_in_subject].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(0)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([])
        end
      end
    end

    describe "when errors should be trated as warnings" do
      describe "where there are no trailers" do
        it "reports errors" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:no_trailers].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check(warn: true)

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(1)
          expect(status_report[:warnings]).to eq([Danger::DangerGfsmCommitTrailer::ERROR_MESSAGE])
          expect(status_report[:errors]).to eq([])
        end
      end

      describe "where there is a single trailer" do
        it "does not report any error" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:single_trailer].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check(warn: true)

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(0)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([])
        end
      end

      describe "where there are multiple trailers" do
        it "does not report any error" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:multiple_trailers].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check(warn: true)

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(0)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([])
        end
      end

      describe "when the trailer is on the subject" do
        it "reports errors" do
          gfsm_commit_trailer = testing_dangerfile.gfsm_commit_trailer
          commits = TEST_MESSAGES[:trailer_in_subject].map do |message|
            double(:commit, message: message, sha: random_sha)
          end

          allow(gfsm_commit_trailer.git).to receive(:commits).and_return(commits)

          gfsm_commit_trailer.check(warn: true)

          status_report = gfsm_commit_trailer.status_report
          expect(report_counts(status_report)).to eq(0)
          expect(status_report[:warnings]).to eq([])
          expect(status_report[:errors]).to eq([])
        end
      end
    end
  end
end
